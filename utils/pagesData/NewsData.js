import news_1 from "../../assets/news/news-1.svg"

export const newsData = [
	{
		image:news_1,
		category:"development",
		date:"december, 2021",
		author:"tom ford",
		title:"How  Web App Can Benefit  Online stores?",
		id:1,
		text:"Elementum Proin Mollis Consectetur Nec. Nam Pretium Eget Sed Pellentesque Nulla Commodo. Cursus Risus Faucibus Mi Elementum Diam Sem Enim. Consectetur Scelerisque Lorem Suspendisse Vel Condimentum Posuere. Nisi, Arcu Integer Morbi Id. Quisque At Eget Donec Quis Tortor. Sit Purus Ut Sed Sed. Fermentum Massa Sit Sapien Sapien Et Facilisis Turpis. Proin Tempus, Quis Nunc Felis Amet, Magna Suspendisse. Diam Libero Urna Augue Dictumst Laoreet At. Nisl Tortor Magna At Enim. Mattis Cursus Nulla Id Semper Vitae Amet, Vulputate Nulla. Nec Nulla A Venenatis Quam Mauris Vitae In Sit. Consectetur Sagittis Quisque Id Mauris Justo Rhoncus. Amet Sed Volutpat Lacus Dolor Eu Pellentesque Proin. Sit Justo, Felis Leo Libero Risus Eget Ut. Elementum Massa Pellentesque Eget Duis Lacinia. Ullamcorper Maecenas Tellus Id Blandit Ipsum Condimentum. Et Viverra Amet Egestas In Cursus. Egestas Massa At Venenatis Enim Sed. Consequat Sit Eu Et Id Dignissim Ac Scelerisque Etiam. A Lacus, Gravida Rutrum Nulla Placerat Porttitor. Rutrum Viverra Tincidunt Fermentum Aliquam Elit, Pellentesque Sit. Accumsan Id Suscipit Facilisi Arcu Nibh. Odio Accumsan, Ac Dignissim Purus Neque Augue Malesuada Nisl Tincidunt. Tempus Eu Pharetra, Mauris, Blandit Amet. In Elementum Semper Faucibus Cursus Massa. Sagittis Varius Turpis Potenti Accumsan Volutpat Proin Cursus Arcu Neque."
	},
	{
		image:news_1,
		category:"development",
		date:"december, 2021",
		author:"tom ford",
		title:"How  Web App Can Benefit  Online stores?",
		id:2,
		text:"Ernazar Elementum Proin Mollis Consectetur Nec. Nam Pretium Eget Sed Pellentesque Nulla Commodo. Cursus Risus Faucibus Mi Elementum Diam Sem Enim. Consectetur Scelerisque Lorem Suspendisse Vel Condimentum Posuere. Nisi, Arcu Integer Morbi Id. Quisque At Eget Donec Quis Tortor. Sit Purus Ut Sed Sed. Fermentum Massa Sit Sapien Sapien Et Facilisis Turpis. Proin Tempus, Quis Nunc Felis Amet, Magna Suspendisse. Diam Libero Urna Augue Dictumst Laoreet At. Nisl Tortor Magna At Enim. Mattis Cursus Nulla Id Semper Vitae Amet, Vulputate Nulla. Nec Nulla A Venenatis Quam Mauris Vitae In Sit. Consectetur Sagittis Quisque Id Mauris Justo Rhoncus. Amet Sed Volutpat Lacus Dolor Eu Pellentesque Proin. Sit Justo, Felis Leo Libero Risus Eget Ut. Elementum Massa Pellentesque Eget Duis Lacinia. Ullamcorper Maecenas Tellus Id Blandit Ipsum Condimentum. Et Viverra Amet Egestas In Cursus. Egestas Massa At Venenatis Enim Sed. Consequat Sit Eu Et Id Dignissim Ac Scelerisque Etiam. A Lacus, Gravida Rutrum Nulla Placerat Porttitor. Rutrum Viverra Tincidunt Fermentum Aliquam Elit, Pellentesque Sit. Accumsan Id Suscipit Facilisi Arcu Nibh. Odio Accumsan, Ac Dignissim Purus Neque Augue Malesuada Nisl Tincidunt. Tempus Eu Pharetra, Mauris, Blandit Amet. In Elementum Semper Faucibus Cursus Massa. Sagittis Varius Turpis Potenti Accumsan Volutpat Proin Cursus Arcu Neque."
	},{
		image:news_1,
		category:"development",
		date:"december, 2021",
		author:"tom ford",
		title:"How  Web App Can Benefit  Online stores?",
		id:3,
		text:"Elementum Proin Mollis Consectetur Nec. Nam Pretium Eget Sed Pellentesque Nulla Commodo. Cursus Risus Faucibus Mi Elementum Diam Sem Enim. Consectetur Scelerisque Lorem Suspendisse Vel Condimentum Posuere. Nisi, Arcu Integer Morbi Id. Quisque At Eget Donec Quis Tortor. Sit Purus Ut Sed Sed. Fermentum Massa Sit Sapien Sapien Et Facilisis Turpis. Proin Tempus, Quis Nunc Felis Amet, Magna Suspendisse. Diam Libero Urna Augue Dictumst Laoreet At. Nisl Tortor Magna At Enim. Mattis Cursus Nulla Id Semper Vitae Amet, Vulputate Nulla. Nec Nulla A Venenatis Quam Mauris Vitae In Sit. Consectetur Sagittis Quisque Id Mauris Justo Rhoncus. Amet Sed Volutpat Lacus Dolor Eu Pellentesque Proin. Sit Justo, Felis Leo Libero Risus Eget Ut. Elementum Massa Pellentesque Eget Duis Lacinia. Ullamcorper Maecenas Tellus Id Blandit Ipsum Condimentum. Et Viverra Amet Egestas In Cursus. Egestas Massa At Venenatis Enim Sed. Consequat Sit Eu Et Id Dignissim Ac Scelerisque Etiam. A Lacus, Gravida Rutrum Nulla Placerat Porttitor. Rutrum Viverra Tincidunt Fermentum Aliquam Elit, Pellentesque Sit. Accumsan Id Suscipit Facilisi Arcu Nibh. Odio Accumsan, Ac Dignissim Purus Neque Augue Malesuada Nisl Tincidunt. Tempus Eu Pharetra, Mauris, Blandit Amet. In Elementum Semper Faucibus Cursus Massa. Sagittis Varius Turpis Potenti Accumsan Volutpat Proin Cursus Arcu Neque."
	},
	{
		image:news_1,
		category:"development",
		date:"december, 2021",
		author:"tom ford",
		title:"How  Web App Can Benefit  Online stores?",
		id:4,
		text:"Elementum Proin Mollis Consectetur Nec. Nam Pretium Eget Sed Pellentesque Nulla Commodo. Cursus Risus Faucibus Mi Elementum Diam Sem Enim. Consectetur Scelerisque Lorem Suspendisse Vel Condimentum Posuere. Nisi, Arcu Integer Morbi Id. Quisque At Eget Donec Quis Tortor. Sit Purus Ut Sed Sed. Fermentum Massa Sit Sapien Sapien Et Facilisis Turpis. Proin Tempus, Quis Nunc Felis Amet, Magna Suspendisse. Diam Libero Urna Augue Dictumst Laoreet At. Nisl Tortor Magna At Enim. Mattis Cursus Nulla Id Semper Vitae Amet, Vulputate Nulla. Nec Nulla A Venenatis Quam Mauris Vitae In Sit. Consectetur Sagittis Quisque Id Mauris Justo Rhoncus. Amet Sed Volutpat Lacus Dolor Eu Pellentesque Proin. Sit Justo, Felis Leo Libero Risus Eget Ut. Elementum Massa Pellentesque Eget Duis Lacinia. Ullamcorper Maecenas Tellus Id Blandit Ipsum Condimentum. Et Viverra Amet Egestas In Cursus. Egestas Massa At Venenatis Enim Sed. Consequat Sit Eu Et Id Dignissim Ac Scelerisque Etiam. A Lacus, Gravida Rutrum Nulla Placerat Porttitor. Rutrum Viverra Tincidunt Fermentum Aliquam Elit, Pellentesque Sit. Accumsan Id Suscipit Facilisi Arcu Nibh. Odio Accumsan, Ac Dignissim Purus Neque Augue Malesuada Nisl Tincidunt. Tempus Eu Pharetra, Mauris, Blandit Amet. In Elementum Semper Faucibus Cursus Massa. Sagittis Varius Turpis Potenti Accumsan Volutpat Proin Cursus Arcu Neque."
	},
	{
		image:news_1,
		category:"development",
		date:"december, 2021",
		author:"tom ford",
		title:"How  Web App Can Benefit  Online stores?",
		id:5,
		text:"Elementum Proin Mollis Consectetur Nec. Nam Pretium Eget Sed Pellentesque Nulla Commodo. Cursus Risus Faucibus Mi Elementum Diam Sem Enim. Consectetur Scelerisque Lorem Suspendisse Vel Condimentum Posuere. Nisi, Arcu Integer Morbi Id. Quisque At Eget Donec Quis Tortor. Sit Purus Ut Sed Sed. Fermentum Massa Sit Sapien Sapien Et Facilisis Turpis. Proin Tempus, Quis Nunc Felis Amet, Magna Suspendisse. Diam Libero Urna Augue Dictumst Laoreet At. Nisl Tortor Magna At Enim. Mattis Cursus Nulla Id Semper Vitae Amet, Vulputate Nulla. Nec Nulla A Venenatis Quam Mauris Vitae In Sit. Consectetur Sagittis Quisque Id Mauris Justo Rhoncus. Amet Sed Volutpat Lacus Dolor Eu Pellentesque Proin. Sit Justo, Felis Leo Libero Risus Eget Ut. Elementum Massa Pellentesque Eget Duis Lacinia. Ullamcorper Maecenas Tellus Id Blandit Ipsum Condimentum. Et Viverra Amet Egestas In Cursus. Egestas Massa At Venenatis Enim Sed. Consequat Sit Eu Et Id Dignissim Ac Scelerisque Etiam. A Lacus, Gravida Rutrum Nulla Placerat Porttitor. Rutrum Viverra Tincidunt Fermentum Aliquam Elit, Pellentesque Sit. Accumsan Id Suscipit Facilisi Arcu Nibh. Odio Accumsan, Ac Dignissim Purus Neque Augue Malesuada Nisl Tincidunt. Tempus Eu Pharetra, Mauris, Blandit Amet. In Elementum Semper Faucibus Cursus Massa. Sagittis Varius Turpis Potenti Accumsan Volutpat Proin Cursus Arcu Neque."
	},
	{
		image:news_1,
		category:"development",
		date:"december, 2021",
		author:"tom ford",
		title:"How  Web App Can Benefit  Online stores?",
		id:6,
		text:"Elementum Proin Mollis Consectetur Nec. Nam Pretium Eget Sed Pellentesque Nulla Commodo. Cursus Risus Faucibus Mi Elementum Diam Sem Enim. Consectetur Scelerisque Lorem Suspendisse Vel Condimentum Posuere. Nisi, Arcu Integer Morbi Id. Quisque At Eget Donec Quis Tortor. Sit Purus Ut Sed Sed. Fermentum Massa Sit Sapien Sapien Et Facilisis Turpis. Proin Tempus, Quis Nunc Felis Amet, Magna Suspendisse. Diam Libero Urna Augue Dictumst Laoreet At. Nisl Tortor Magna At Enim. Mattis Cursus Nulla Id Semper Vitae Amet, Vulputate Nulla. Nec Nulla A Venenatis Quam Mauris Vitae In Sit. Consectetur Sagittis Quisque Id Mauris Justo Rhoncus. Amet Sed Volutpat Lacus Dolor Eu Pellentesque Proin. Sit Justo, Felis Leo Libero Risus Eget Ut. Elementum Massa Pellentesque Eget Duis Lacinia. Ullamcorper Maecenas Tellus Id Blandit Ipsum Condimentum. Et Viverra Amet Egestas In Cursus. Egestas Massa At Venenatis Enim Sed. Consequat Sit Eu Et Id Dignissim Ac Scelerisque Etiam. A Lacus, Gravida Rutrum Nulla Placerat Porttitor. Rutrum Viverra Tincidunt Fermentum Aliquam Elit, Pellentesque Sit. Accumsan Id Suscipit Facilisi Arcu Nibh. Odio Accumsan, Ac Dignissim Purus Neque Augue Malesuada Nisl Tincidunt. Tempus Eu Pharetra, Mauris, Blandit Amet. In Elementum Semper Faucibus Cursus Massa. Sagittis Varius Turpis Potenti Accumsan Volutpat Proin Cursus Arcu Neque."
	},
]