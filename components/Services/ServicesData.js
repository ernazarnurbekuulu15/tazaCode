import web from "../../assets/services/wev-development.svg"
import mobile from "../../assets/services/mobile-development.svg"
import ui_ux from "../../assets/services/ui-ux.svg"
import QA from "../../assets/services/QA-TEST.svg"
import consulting from "../../assets/services/consulting.svg"
import team from "../../assets/services/team.svg"


export const servicesData = [
	{
		logo:web,
		title:"web development",
		description:"lorem ipsum dolor sit amet, consectetur adipiscing elit ut "
	},	{
		logo:mobile,
		title:"mobile development",
		description:"lorem ipsum dolor sit amet, consectetur adipiscing elit ut "
	},	{
		logo:ui_ux,
		title:"ui/ux design",
		description:"lorem ipsum dolor sit amet, consectetur adipiscing elit ut "
	},	{
		logo:QA,
		title:"qa & testing",
		description:"lorem ipsum dolor sit amet, consectetur adipiscing elit ut "
	},{
		logo:consulting,
		title:"it consultancy",
		description:"lorem ipsum dolor sit amet, consectetur adipiscing elit ut "
	},{
		logo:team,
		title:"dedicated team",
		description:"lorem ipsum dolor sit amet, consectetur adipiscing elit ut "
	},
]